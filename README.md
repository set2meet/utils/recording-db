This lib all functionality relative to db and recording witch using in different projects

####How to use

1. config  
   currently supports just one provider based on redis, it will be selected by config key `redisConfig`:
   ```
   "redisConfig": {
     "port": number,
     "host": string
   }
   ```
2. import
   ```
   import { getRecordingDb, getRecordingSubPub, getDistributedLock } from '@s2m/recording-db';
   ```
