import { ServerLogger, S2MServices, DefaultModules } from '@s2m/logger';
import { ELogErrorCode } from './ELogErrorCode';

const appLogger: ServerLogger<DefaultModules, ELogErrorCode> = new ServerLogger(S2MServices.RecordingDb);

export default appLogger;
