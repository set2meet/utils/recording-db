import IRecordingDb from './IRecordingDb';
import IRecordingSubPub from './IRecordingSubPub';
import IDistributedLock from './IDistributedLock';

export { IRecordingDb, IRecordingSubPub, IDistributedLock };

export enum RecordItemStatus {
  removing = 'removing',
  failed = 'failed',
  available = 'available',
  recording = 'recording',
  processing = 'processing',
}

export type TRecordItem = {
  id: string;
  src: string;
  createdAt: Date;
  createdBy: string;
  expiredAt: Date;
  roomId: string;
  realmName: string;
  status: RecordItemStatus;
  duration: number | string; // ms
  size: string;
  resolution: string;
  subscribedTo: string;
  name: string;
  type: string;
  meta?: {
    /**view (see meeting in recording list and can also view it)
    delete (see delete button in gui and can use it)*/
    isOwner: boolean;
    /**readonly view (see meeting in recording list and can also view it)*/
    isViewer: boolean;
  };
};

export const RECORDING_CHANNEL = 'recording_channel';

export enum RecordingMessageType {
  RECORDING_REMOVED = 'RECORDING_REMOVED',
  RECORDING_UPDATED = 'RECORDING_UPDATED',
}

export type TRecordingMessage = {
  type: RecordingMessageType;
  record: TRecordItem;
  participants: string[];
};

export type TRecordingMessageListener = (message: TRecordingMessage) => void;
