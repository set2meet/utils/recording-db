import { TRecordItem } from './types';

interface IRecordingDb {
  getRecord(id: string): Promise<TRecordItem>;

  deleteRecord(id: string): Promise<void>;

  updateRecord(id: string, patch: Partial<TRecordItem>): Promise<void>;

  isRecordExists(id: string): Promise<boolean>;
}

export default IRecordingDb;
