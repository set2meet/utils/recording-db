import IDistributedLock from './IDistributedLock';
import { getDistributedLock as getRedisDistributedLock } from './redis/services';

let distributedLock: IDistributedLock = null;

export const getDistributedLock = (): IDistributedLock => {
  distributedLock = getRedisDistributedLock();

  if (!distributedLock) {
    throw new Error('DistributedLock is not configured.');
  }

  return distributedLock;
};

export default getDistributedLock;
