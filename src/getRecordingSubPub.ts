import IRecordingSubPub from './IRecordingSubPub';
import { getRecordingSubPub as getRedisRecordingSubPub } from './redis/services';

let recordingSubPub: IRecordingSubPub = null;

const getRecordingSubPub = (): IRecordingSubPub => {
  recordingSubPub = getRedisRecordingSubPub();

  if (!recordingSubPub) {
    throw new Error('RecordingSubPub is not configured.');
  }

  return recordingSubPub;
};

export default getRecordingSubPub;
