import getDistributedLock from '../getDistributedLock';
import getRecordingDb from '../getRecordingDb';
import getRecordingSubPub from '../getRecordingSubPub';
import IDistributedLock from '../IDistributedLock';
import IRecordingSubPub from '../IRecordingSubPub';
import IRecordingDb from '../IRecordingDb';
import { mock } from 'jest-mock-extended';
import {
  getDistributedLock as getRedisDistributedLock,
  getRecordingSubPub as getRedisRecordingSubPub,
  getRecordingDb as getRedisRecordingDb,
} from '../redis/services';

const mockDistributedLock = mock<IDistributedLock>();
const mockRecordingSubPub = mock<IRecordingSubPub>();
const mockRecordingDb = mock<IRecordingDb>();

jest.mock('../redis/services', () => ({
  getDistributedLock: jest.fn(),
  getRecordingSubPub: jest.fn(),
  getRecordingDb: jest.fn(),
}));

describe('redis service provider', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  test('configured', () => {
    (getRedisDistributedLock as jest.Mock).mockReturnValue(mockDistributedLock);
    (getRedisRecordingSubPub as jest.Mock).mockReturnValue(mockRecordingSubPub);
    (getRedisRecordingDb as jest.Mock).mockReturnValue(mockRecordingDb);

    expect(getDistributedLock()).toBeDefined();
    expect(getRecordingDb()).toBeDefined();
    expect(getRecordingSubPub()).toBeDefined();
  });

  test('not configured', () => {
    expect(() => {
      getDistributedLock();
    }).toThrowError();

    expect(() => {
      getRecordingDb();
    }).toThrowError();

    expect(() => {
      getRecordingSubPub();
    }).toThrowError();
  });
});
