import IRecordingDb from './IRecordingDb';
import { getRecordingDb as getRedisRecordingDb } from './redis/services';

let recordingDb: IRecordingDb = null;

export const getRecordingDb = (): IRecordingDb => {
  recordingDb = getRedisRecordingDb();

  if (!recordingDb) {
    throw new Error('RecordingDb is not configured.');
  }

  return recordingDb;
};

export default getRecordingDb;
