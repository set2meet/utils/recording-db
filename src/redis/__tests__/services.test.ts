import { getRecordingDb, getRecordingSubPub, getDistributedLock } from '../services';

describe('services', () => {
  test('for redis should return one instance', () => {
    const recordingDb = getRecordingDb();
    const recordingSubPub = getRecordingSubPub();
    const distributedLock = getDistributedLock();

    expect(recordingDb).toBe(recordingSubPub);
    expect(recordingDb).toBe(distributedLock);
  });
});
