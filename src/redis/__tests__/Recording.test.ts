import IORedis, { KeyType } from 'ioredis';
import { mock } from 'jest-mock-extended';
import { RECORDING_CHANNEL, RecordingMessageType, RecordItemStatus, TRecordingMessage, TRecordItem } from '../../types';
import Recording from '../Recording';

/* eslint-disable @typescript-eslint/unbound-method */

const mockRedis = mock<IORedis.Redis>();

jest.mock('ioredis', () => jest.fn().mockImplementation(() => mockRedis));

// eslint-disable-next-line max-statements
describe('', () => {
  let recording: Recording = null;

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('test lazy & pub/sub', async () => {
    recording = new Recording({
      port: 1,
      host: '0.0.0.0',
    });

    mockRedis.subscribe.mockResolvedValue(null);

    await recording.subscribeOnChannel(jest.fn());
    await recording.subscribeOnChannel(jest.fn());

    expect(mockRedis.subscribe).toBeCalledTimes(1);
    expect(IORedis).toHaveBeenNthCalledWith(1, {
      port: 1,
      host: '0.0.0.0',
    });

    await recording.publishToChannel(mock<TRecordingMessage>());
    await recording.publishToChannel(mock<TRecordingMessage>());

    //eslint-disable-next-line no-magic-numbers
    expect(IORedis).toHaveBeenNthCalledWith(2, {
      port: 1,
      host: '0.0.0.0',
    });

    //eslint-disable-next-line no-magic-numbers
    expect(IORedis).toBeCalledTimes(2);
  });

  test('updateRecord', async () => {
    mockRedis.hmset.mockResolvedValue(null);

    await recording.updateRecord('id', { name: 'name1', status: RecordItemStatus.processing });

    expect(mockRedis.hmset).toBeCalledWith(
      'recording:records:id',
      'name',
      'name1',
      'status',
      RecordItemStatus.processing
    );
  });

  test('subscribe', async () => {
    mockRedis.subscribe.mockResolvedValue(null);

    const listener = jest.fn();
    const message: TRecordingMessage = {
      type: RecordingMessageType.RECORDING_UPDATED,
      record: { id: 'id1', name: 'recordId' } as TRecordItem,
      participants: ['user1', 'user2'],
    };

    await recording.subscribeOnChannel(listener);

    const handler = mockRedis.on.mock.calls[0][1];

    handler(RECORDING_CHANNEL, JSON.stringify(message));
    handler(RECORDING_CHANNEL, JSON.stringify(message));
    handler('some another channel', 'some data');

    expect(listener).toHaveBeenNthCalledWith(1, message);
    //eslint-disable-next-line no-magic-numbers
    expect(listener).toHaveBeenNthCalledWith(2, message);
  });

  test('publish', async () => {
    const message: TRecordingMessage = {
      type: RecordingMessageType.RECORDING_UPDATED,
      record: { id: 'id1', name: 'recordId' } as TRecordItem,
      participants: ['user1', 'user2'],
    };

    await recording.publishToChannel(message);
    await recording.publishToChannel(message);

    expect(mockRedis.publish).toHaveBeenNthCalledWith(1, RECORDING_CHANNEL, JSON.stringify(message));
    //eslint-disable-next-line no-magic-numbers
    expect(mockRedis.publish).toHaveBeenNthCalledWith(2, RECORDING_CHANNEL, JSON.stringify(message));
  });

  test('isRecordExists', async () => {
    mockRedis.exists.mockResolvedValue(1 as never);

    const isExists = await recording.isRecordExists('id');

    expect(mockRedis.exists as (...keys: KeyType[]) => Promise<number>).toBeCalledWith('recording:records:id');
    expect(isExists).toBe(true);
  });

  test('getRecord', async () => {
    const mockRecord = mock<Record<string, string>>({
      id: 'id1',
      name: 'name1',
    });

    mockRedis.hgetall.mockResolvedValue(mockRecord);

    const record1 = await recording.getRecord('id');
    const record2 = await recording.getRecord('id');

    expect(mockRedis.hgetall).toBeCalledWith('recording:records:id');
    expect(record1.id).toBe(mockRecord['id']);
    expect(record1.name).toBe(mockRecord['name']);
    expect(record2.id).toBe(mockRecord['id']);
    expect(record2.name).toBe(mockRecord['name']);
  });

  test('deleteRecord', async () => {
    mockRedis.del.mockResolvedValue(null);

    await recording.deleteRecord('id');

    expect(mockRedis.del as (...keys: KeyType[]) => Promise<number>).toBeCalledWith('recording:records:id');
  });

  test('acquireLock', async () => {
    (mockRedis.set as jest.Mock).mockResolvedValueOnce('OK').mockResolvedValueOnce(0);

    const ttlSec = 20;
    const isLockAdded1 = await recording.acquireLock('name1', ttlSec);
    const isLockAdded2 = await recording.acquireLock('name2', ttlSec);

    expect(mockRedis.set.mock.calls[0]).toEqual(['lock__name1', 'name1', 'EX', ttlSec, 'NX']);
    expect(mockRedis.set.mock.calls[1]).toEqual(['lock__name2', 'name2', 'EX', ttlSec, 'NX']);
    expect(isLockAdded1).toBeTruthy();
    expect(isLockAdded2).toBeFalsy();
  });

  test('releaseLock', async () => {
    (mockRedis.get as jest.Mock).mockResolvedValueOnce(0).mockResolvedValue('name2');

    const isReleased1 = await recording.releaseLock('name1');
    const isReleased2 = await recording.releaseLock('name2');

    expect(isReleased1).toBeFalsy();
    expect(isReleased2).toBeTruthy();
    expect(mockRedis.del).toBeCalledTimes(1);
    expect(mockRedis.del as (...keys: KeyType[]) => Promise<number>).toBeCalledWith('lock__name2');
  });
});
