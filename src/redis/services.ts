import { default as Recording, TRedisConfig } from './Recording';
import config from 'config';
import IRecordingDb from '../IRecordingDb';
import IRecordingSubPub from '../IRecordingSubPub';
import IDistributedLock from '../IDistributedLock';

type TRecording = IRecordingDb & IRecordingSubPub & IDistributedLock;

let recording: TRecording = null;

const getRecording = (): TRecording => {
  if (!recording && config.has('redisConfig')) {
    recording = new Recording(config.get<TRedisConfig>('redisConfig'));
  }

  return recording;
};

export const getRecordingDb = (): IRecordingDb => getRecording();

export const getRecordingSubPub = (): IRecordingSubPub => getRecording();

export const getDistributedLock = (): IDistributedLock => getRecording();
