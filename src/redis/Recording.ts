import IORedis, { ValueType } from 'ioredis';
import logger from '../logger/loggerProvider';
import { RECORDING_CHANNEL, TRecordingMessage, TRecordItem, TRecordingMessageListener } from '../types';
import IRecordingDb from '../IRecordingDb';
import IRecordingSubPub from '../IRecordingSubPub';
import IDistributedLock from '../IDistributedLock';

const REDIS_BASIC_KEY = 'recording';
const REDIS_RECORDS = `${REDIS_BASIC_KEY}:records`;
const REDIS_RECORD_KEY = (id: string) => `${REDIS_RECORDS}:${id}`;

export type TRedisConfig = {
  port: number;
  host: string;
};

const buildLockKeyName = (name: string): string => `lock__${name}`;

export default class Recording implements IRecordingDb, IRecordingSubPub, IDistributedLock {
  private _pubRedis: IORedis.Redis;
  private _subRedis: IORedis.Redis;
  private readonly config: TRedisConfig;

  constructor(config: TRedisConfig) {
    this.config = config;
  }

  private pubRedis() {
    if (!this._pubRedis) {
      this._pubRedis = new IORedis(this.config);
    }

    return this._pubRedis;
  }

  private async subRedis() {
    if (!this._subRedis) {
      this._subRedis = new IORedis(this.config);

      await this._subRedis.subscribe(RECORDING_CHANNEL);
    }

    return this._subRedis;
  }

  public async getRecord(id: string): Promise<TRecordItem> {
    const key = REDIS_RECORD_KEY(id);

    logger.debug(`read record with key = ${key}`);

    const rawRecord = await this.pubRedis().hgetall(key);
    const record = rawRecord as Partial<TRecordItem>;

    record.id = id;

    return record as TRecordItem;
  }

  public async deleteRecord(id: string): Promise<void> {
    const key = REDIS_RECORD_KEY(id);

    logger.debug(`delete record with key = ${key}`);

    await this.pubRedis().del(key);
  }

  public async updateRecord(id: string, patch: Partial<TRecordItem>): Promise<void> {
    const key = REDIS_RECORD_KEY(id);

    logger.debug(`update record with key = ${key}`);

    const values: ValueType[] = Object.entries(patch)
      .flat()
      .map((val) => `${val as unknown}`); //eslint-disable-line @typescript-eslint/restrict-template-expressions

    await this.pubRedis().hmset(key, ...values);
  }

  public async isRecordExists(id: string): Promise<boolean> {
    return !!(await this.pubRedis().exists(REDIS_RECORD_KEY(id)));
  }

  public async subscribeOnChannel(fn: TRecordingMessageListener): Promise<void> {
    (await this.subRedis()).on('message', (channel: string, message: string) => {
      if (channel === RECORDING_CHANNEL) {
        logger.debug(`received message ${message}`);

        const recordingMessage = JSON.parse(message) as TRecordingMessage;

        fn(recordingMessage);
      }
    });
  }

  public async publishToChannel(message: TRecordingMessage): Promise<number> {
    const serializedMessage = JSON.stringify(message);

    logger.debug(`send message ${serializedMessage}`);

    return this.pubRedis().publish(RECORDING_CHANNEL, serializedMessage);
  }

  public async acquireLock(name: string, ttl: number): Promise<boolean> {
    const keyName = buildLockKeyName(name);
    const result = await this.pubRedis().set(keyName, name, 'EX', ttl, 'NX');
    const isLockAdded = result === 'OK';

    if (isLockAdded) {
      logger.debug(`added lock for key ${name} and ttl ${ttl}`);
    } else {
      logger.debug(`cannot adding lock for key ${name} with ttl ${ttl}`);
    }

    return isLockAdded;
  }

  public async releaseLock(name: string): Promise<boolean> {
    const keyName = buildLockKeyName(name);
    const value = await this.pubRedis().get(keyName);

    if (name === value) {
      await this.pubRedis().del(keyName);

      logger.debug(`release lock for key ${name}`);

      return true;
    }

    logger.debug(`error while releasing lock for key ${name}`);

    return false;
  }
}
