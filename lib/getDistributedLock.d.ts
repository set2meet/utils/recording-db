import IDistributedLock from './IDistributedLock';
export declare const getDistributedLock: () => IDistributedLock;
export default getDistributedLock;
