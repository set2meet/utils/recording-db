"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDistributedLock = void 0;
const services_1 = require("./redis/services");
let distributedLock = null;
exports.getDistributedLock = () => {
    distributedLock = services_1.getDistributedLock();
    if (!distributedLock) {
        throw new Error('DistributedLock is not configured.');
    }
    return distributedLock;
};
exports.default = exports.getDistributedLock;
