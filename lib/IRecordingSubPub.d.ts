import { TRecordingMessage, TRecordingMessageListener } from './types';
interface IRecordingSubPub {
    subscribeOnChannel(fn: TRecordingMessageListener): Promise<void>;
    publishToChannel(message: TRecordingMessage): Promise<number>;
}
export default IRecordingSubPub;
