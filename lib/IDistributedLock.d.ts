interface IDistributedLock {
    acquireLock(name: string, ttl: number): Promise<boolean>;
    releaseLock(name: string): Promise<boolean>;
}
export default IDistributedLock;
