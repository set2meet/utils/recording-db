"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRecordingDb = void 0;
const services_1 = require("./redis/services");
let recordingDb = null;
exports.getRecordingDb = () => {
    recordingDb = services_1.getRecordingDb();
    if (!recordingDb) {
        throw new Error('RecordingDb is not configured.');
    }
    return recordingDb;
};
exports.default = exports.getRecordingDb;
