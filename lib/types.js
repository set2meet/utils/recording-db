"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecordingMessageType = exports.RECORDING_CHANNEL = exports.RecordItemStatus = void 0;
var RecordItemStatus;
(function (RecordItemStatus) {
    RecordItemStatus["removing"] = "removing";
    RecordItemStatus["failed"] = "failed";
    RecordItemStatus["available"] = "available";
    RecordItemStatus["recording"] = "recording";
    RecordItemStatus["processing"] = "processing";
})(RecordItemStatus = exports.RecordItemStatus || (exports.RecordItemStatus = {}));
exports.RECORDING_CHANNEL = 'recording_channel';
var RecordingMessageType;
(function (RecordingMessageType) {
    RecordingMessageType["RECORDING_REMOVED"] = "RECORDING_REMOVED";
    RecordingMessageType["RECORDING_UPDATED"] = "RECORDING_UPDATED";
})(RecordingMessageType = exports.RecordingMessageType || (exports.RecordingMessageType = {}));
