"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDistributedLock = exports.getRecordingSubPub = exports.getRecordingDb = void 0;
const Recording_1 = __importDefault(require("./Recording"));
const config_1 = __importDefault(require("config"));
let recording = null;
const getRecording = () => {
    if (!recording && config_1.default.has('redisConfig')) {
        recording = new Recording_1.default(config_1.default.get('redisConfig'));
    }
    return recording;
};
exports.getRecordingDb = () => getRecording();
exports.getRecordingSubPub = () => getRecording();
exports.getDistributedLock = () => getRecording();
