import IRecordingDb from '../IRecordingDb';
import IRecordingSubPub from '../IRecordingSubPub';
import IDistributedLock from '../IDistributedLock';
export declare const getRecordingDb: () => IRecordingDb;
export declare const getRecordingSubPub: () => IRecordingSubPub;
export declare const getDistributedLock: () => IDistributedLock;
