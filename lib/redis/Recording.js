"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ioredis_1 = __importDefault(require("ioredis"));
const loggerProvider_1 = __importDefault(require("../logger/loggerProvider"));
const types_1 = require("../types");
const REDIS_BASIC_KEY = 'recording';
const REDIS_RECORDS = `${REDIS_BASIC_KEY}:records`;
const REDIS_RECORD_KEY = (id) => `${REDIS_RECORDS}:${id}`;
const buildLockKeyName = (name) => `lock__${name}`;
class Recording {
    constructor(config) {
        this.config = config;
    }
    pubRedis() {
        if (!this._pubRedis) {
            this._pubRedis = new ioredis_1.default(this.config);
        }
        return this._pubRedis;
    }
    subRedis() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._subRedis) {
                this._subRedis = new ioredis_1.default(this.config);
                yield this._subRedis.subscribe(types_1.RECORDING_CHANNEL);
            }
            return this._subRedis;
        });
    }
    getRecord(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const key = REDIS_RECORD_KEY(id);
            loggerProvider_1.default.debug(`read record with key = ${key}`);
            const rawRecord = yield this.pubRedis().hgetall(key);
            const record = rawRecord;
            record.id = id;
            return record;
        });
    }
    deleteRecord(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const key = REDIS_RECORD_KEY(id);
            loggerProvider_1.default.debug(`delete record with key = ${key}`);
            yield this.pubRedis().del(key);
        });
    }
    updateRecord(id, patch) {
        return __awaiter(this, void 0, void 0, function* () {
            const key = REDIS_RECORD_KEY(id);
            loggerProvider_1.default.debug(`update record with key = ${key}`);
            const values = Object.entries(patch)
                .flat()
                .map((val) => `${val}`); //eslint-disable-line @typescript-eslint/restrict-template-expressions
            yield this.pubRedis().hmset(key, ...values);
        });
    }
    isRecordExists(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return !!(yield this.pubRedis().exists(REDIS_RECORD_KEY(id)));
        });
    }
    subscribeOnChannel(fn) {
        return __awaiter(this, void 0, void 0, function* () {
            (yield this.subRedis()).on('message', (channel, message) => {
                if (channel === types_1.RECORDING_CHANNEL) {
                    loggerProvider_1.default.debug(`received message ${message}`);
                    const recordingMessage = JSON.parse(message);
                    fn(recordingMessage);
                }
            });
        });
    }
    publishToChannel(message) {
        return __awaiter(this, void 0, void 0, function* () {
            const serializedMessage = JSON.stringify(message);
            loggerProvider_1.default.debug(`send message ${serializedMessage}`);
            return this.pubRedis().publish(types_1.RECORDING_CHANNEL, serializedMessage);
        });
    }
    acquireLock(name, ttl) {
        return __awaiter(this, void 0, void 0, function* () {
            const keyName = buildLockKeyName(name);
            const result = yield this.pubRedis().set(keyName, name, 'EX', ttl, 'NX');
            const isLockAdded = result === 'OK';
            if (isLockAdded) {
                loggerProvider_1.default.debug(`added lock for key ${name} and ttl ${ttl}`);
            }
            else {
                loggerProvider_1.default.debug(`cannot adding lock for key ${name} with ttl ${ttl}`);
            }
            return isLockAdded;
        });
    }
    releaseLock(name) {
        return __awaiter(this, void 0, void 0, function* () {
            const keyName = buildLockKeyName(name);
            const value = yield this.pubRedis().get(keyName);
            if (name === value) {
                yield this.pubRedis().del(keyName);
                loggerProvider_1.default.debug(`release lock for key ${name}`);
                return true;
            }
            loggerProvider_1.default.debug(`error while releasing lock for key ${name}`);
            return false;
        });
    }
}
exports.default = Recording;
