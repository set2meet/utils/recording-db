import { TRecordingMessage, TRecordItem, TRecordingMessageListener } from '../types';
import IRecordingDb from '../IRecordingDb';
import IRecordingSubPub from '../IRecordingSubPub';
import IDistributedLock from '../IDistributedLock';
export declare type TRedisConfig = {
    port: number;
    host: string;
};
export default class Recording implements IRecordingDb, IRecordingSubPub, IDistributedLock {
    private _pubRedis;
    private _subRedis;
    private readonly config;
    constructor(config: TRedisConfig);
    private pubRedis;
    private subRedis;
    getRecord(id: string): Promise<TRecordItem>;
    deleteRecord(id: string): Promise<void>;
    updateRecord(id: string, patch: Partial<TRecordItem>): Promise<void>;
    isRecordExists(id: string): Promise<boolean>;
    subscribeOnChannel(fn: TRecordingMessageListener): Promise<void>;
    publishToChannel(message: TRecordingMessage): Promise<number>;
    acquireLock(name: string, ttl: number): Promise<boolean>;
    releaseLock(name: string): Promise<boolean>;
}
