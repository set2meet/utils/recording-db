"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const services_1 = require("./redis/services");
let recordingSubPub = null;
const getRecordingSubPub = () => {
    recordingSubPub = services_1.getRecordingSubPub();
    if (!recordingSubPub) {
        throw new Error('RecordingSubPub is not configured.');
    }
    return recordingSubPub;
};
exports.default = getRecordingSubPub;
