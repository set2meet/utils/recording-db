import { ServerLogger, DefaultModules } from '@s2m/logger';
import { ELogErrorCode } from './ELogErrorCode';
declare const appLogger: ServerLogger<DefaultModules, ELogErrorCode>;
export default appLogger;
