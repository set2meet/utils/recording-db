"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("@s2m/logger");
const appLogger = new logger_1.ServerLogger(logger_1.S2MServices.RecordingDb);
exports.default = appLogger;
